# `stackwatch.py`

The stackoverflow question watcher that goes "beeep" in your terminal.

## Installation

Make sure to use python 2.7.6+.

    pip install -r requirements.txt

## Usage

Create a `weights.json` file in the same directory and
then simply run the script like so:

    python stackwatch.py default nitrousio python django meteor ruby rails

If you want to name your weight file differently, then change `default`, to
the name of your weight file. Like so:

    python stackwatch.py nitroussupport.json nitrousio python meteor

Enjoy :)