# encoding=utf-8

import json
import logging
import re
import sys

from threading import Thread
from threading import Lock

from bs4 import BeautifulSoup
from websocket import create_connection


class CachedProperty(object):
    """
    A property that is only computed once per instance and then replaces
    itself with an ordinary attribute. Deleting the attribute resets the
    property.

    Source: https://github.com/bottlepy/bottle/commit/fa7733e075da0d790d809aa3d2f53071897e6f76
    """

    def __init__(self, func):
        self.__doc__ = getattr(func, '__doc__')
        self.func = func

    def __get__(self, obj, cls):
        if obj is None:
            return self
        value = obj.__dict__[self.func.__name__] = self.func(obj)
        return value


class LockedSet(set):
    def __init__(self):
        self._lock = Lock()
        super(LockedSet, self).__init__()

    def add(self, elem):
        with self._lock:
            super(LockedSet, self).add(elem)

    def remove(self, elem):
        with self._lock:
            super(LockedSet, self).remove(elem)


class WrongDataFormatException(Exception):
    pass


class StackObject(object):
    """
    A common object to all StackObjects since this can and will house many of the shared requirements of both.
    """
    _BASE_URL = 'http://stackoverflow.com'


class User(StackObject):
    """
    Object that represents a stackoverflow user

    :param name: Name of the user
    :type name: str|unicode

    :param uid: User ID on StackOverflow
    :type uid: int
    """

    def __init__(self, name, uid):
        self.name = name
        self._id = uid

    @CachedProperty
    def url(self):
        """
        :return: The URL for the user's profile page on StackOverflow
        :rtype: str
        """
        return "{}/users/{}".format(self.__class__._BASE_URL, self._id)

    def __repr__(self):
        return "<{}>{{{}}}".format(self.name, self._id)


class Question(StackObject):
    """
    A Question class for StackOverflow questions. Has a class variable called __questions__ that contains all
    instances of questions asked

    :type creator: str
    :param creator: The username of the person who posted the question

    :type name: str
    :param name: The name of the question

    :type id_num: int
    :param id_num: The ID of the question asked

    :param tags: A set of all the associated tags
    :type tags: set
    """
    __questions__ = LockedSet()
    _slash_remover_regex = re.compile(r'\+[nr]?')
    _action_test_regex = re.compile(r'1-questions-newest-tag-\w+')

    def __init__(self, id_num, name, tags, creator=(None, 1)):
        self.id = id_num
        self.tags = tags
        self.name = name
        self.creator = User(*creator)
        self.__class__.__questions__.add(self)

    @CachedProperty
    def weight(self):
        """
        :return: The sum of all the weights of the question's tags
        :rtype: int
        """
        wts = WEIGHTS
        # Not very good complexity
        return sum(wts[ta] for ta in self.tags if ta in wts)

    def __hash__(self):
        return self.id

    @classmethod
    def number_of_questions(cls):
        return len(cls.__questions__)

    @classmethod
    def get_all_questions(cls):
        return cls.__questions__.copy()

    @CachedProperty
    def url(self):
        """
        :return: Returns the stackoverflow url of a question
        :rtype: str
        """
        return "{}/questions/{}".format(self.__class__._BASE_URL, self.id)

    @classmethod
    def from_socket_json(cls, ws_json_string):
        """
        A classmethod which creates a Question object wrapper for
        StackOverflow questions.

        :param ws_json_string: JSON string from web socket
        :type ws_json_string: str|unicode

        :return: A Question Object
        :rtype: Question

        :raise WrongDataFormatException: Websocket returned the wrong JSON string
        """
        info = json.loads(ws_json_string)
        try:
            if cls._action_test_regex.match(info['action']):
                # data contains all the information we need,
                # there are two keys, ``action`` and ``data``
                # the regex removes all the ``\\`` and ``\n`` or ``\r``
                # inside the string
                info = json.loads(cls._slash_remover_regex.sub('', info['data']))
            else:
                raise WrongDataFormatException("Wrong socket data format. Incorrect ``action`` format in JSON object.")
        except KeyError:
            raise

        id_num = int(info['id'])
        tags = set(info['tags'])

        # Getting data from info['body']
        body_soup = BeautifulSoup(info['body'], "html.parser")
        question_name = body_soup.find(
            attrs={'class': 'question-hyperlink'}).text
        cr_name = body_soup.find(
            attrs={'class': 'user-details'}).find('a').text
        cr_id = int(body_soup.find(
            attrs={'class': 'user-details'}).find('a')['href'].split(r'/')[2])

        # Finally creating class
        return cls(id_num=id_num, name=question_name, tags=tags, creator=(cr_name, cr_id))

    def __str__(self):
        return u"{url} \n <{question_name}> \n {tags} \n {creator}".format(
            question_name=self.name,
            creator=self.creator.name,
            url=self.url,
            tags=list(self.tags),
            weight=self.weight
        ).encode('utf-8')

    def __eq__(self, other):
        return self.id == other.id


class StackTagWatcher(Thread):
    def __init__(self, tag, *args, **kwargs):
        super(StackTagWatcher, self).__init__(*args, **kwargs)
        self._socket_message = '1-questions-newest-tag-{}'
        self.tag = tag
        self.daemon = True

    def run(self):
        conn = create_connection('wss://qa.sockets.stackexchange.com')
        conn.send(self._socket_message.format(self.tag))
        while True:
            logging.log(logging.INFO, "{} started".format(self.tag))
            data = conn.recv()
            try:
                Question.from_socket_json(data)
            except WrongDataFormatException as e:
                logging.log(logging.ERROR, e)


def questions():
    size = 0
    while True:
        new_size = Question.number_of_questions()
        if new_size > size:
            size = new_size
            yield Question.get_all_questions()
        else:
            yield None


if __name__ == '__main__':

    args = sys.argv[1:]
    filename = "weights.json" if sys.argv[1] == "default" else sys.argv[1]
    tags = set(args[1:])

    with open(filename) as f:
        WEIGHTS = json.load(f)

    threads = [StackTagWatcher(t) for t in tags]
    for th in threads:
        th.start()
    print("Watching tags {} now".format(", ".join(tags)))

    try:
        for v in questions():
            if v:
                sys.stdout.write("\a")
                print(("#" * 120) + "\n")
                for q in v:
                    print(q)
    except KeyboardInterrupt:
        sys.exit("Goodbye!\n")
